/*
******************************************************************************
**  CarMaker - Version 9.1.1
**  Vehicle Dynamics Simulation Toolkit
**
**  Copyright (C)   IPG Automotive GmbH
**                  Bannwaldallee 60             Phone  +49.721.98520.0
**                  76185 Karlsruhe              Fax    +49.721.98520.99
**                  Germany                      WWW    www.ipg-automotive.com
******************************************************************************
**
** Video Data Stream example client for IPGMovie 3.4 and later versions.
*/

#ifndef __VDS_CLIENT_H__
#define __VDS_CLIENT_H__

#ifdef __cplusplus
extern "C" {
#endif


void VDS_Init  (void);
void VDS_Start (void);
void VDS_Exit  (void);


#ifdef __cplusplus
}
#endif

#endif /* __VDS_CLIENT_H__ */
