/*
******************************************************************************
**  CarMaker - Version 9.1.1
**  Vehicle Dynamics Simulation Toolkit
**
**  Copyright (C)   IPG Automotive GmbH
**                  Bannwaldallee 60             Phone  +49.721.98520.0
**                  76185 Karlsruhe              Fax    +49.721.98520.99
**                  Germany                      WWW    www.ipg-automotive.com
******************************************************************************
**
** Video Data Stream example client for IPGMovie 3.4 and later versions.
**
** This file was the standard example until CM8.0
**
** Compiling:
** Linux
**	gcc -Wall -Os -o vds-client-standalone-basics vds-client-standalone-basics.c
** MS Windows (MSYS/MinGW)
**	gcc -Wall -Os -o vds-client-standalone-basics vds-client-standalone-basics.c -lws2_32
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifdef WIN32
#  include <winsock2.h>
#else
#  include <unistd.h>
#  include <sys/socket.h>
#  include <sys/types.h>
#  include <net/if.h>
#  include <netinet/in.h>
#  include <arpa/inet.h>
#  include <netdb.h>
#endif


static struct {
    char *MovieHost;  /* pc on which IPGMovie runs          */
    int   MoviePort;  /* TCP/IP port for VDS                */
    int   sock;       /* TCP/IP Socket                      */
    char  sbuf[64];   /* Buffer for transmitted information */
    int   RecvFlags;  /* Receive Flags                      */
    int   Verbose;    /* Logging Output                     */
} VDScfg;

struct {
    int         nImages;
    int         nBytes;
    float       MinDepth;
} VDSIF;



/*
** VDS_RecvHdr
**
** Scan TCP/IP Socket and writes to buffer
*/
static int
VDS_RecvHdr (int sock, char *hdr)
{
    const int HdrSize = 64;
    int len = 0;
    int nSkipped = 0;
    int i;

    while (1) {
        for ( ; len < HdrSize; len += i) {
	    if ((i=recv(sock, hdr+len, HdrSize-len, VDScfg.RecvFlags)) <= 0)
		return -1;
	}
	if (hdr[0]=='*' && hdr[1]>='A' && hdr[1]<='Z') {
	    /* remove white spaces at end of line */
	    while (len>0 && hdr[len-1]<=' ')
		len--;
	    hdr[len] = 0;
	    if (VDScfg.Verbose && nSkipped>0)
	        printf ("VDS: HDR resync, %d bytes skipped\n", nSkipped);
	    return 0;
	}
	for (i=1; i<len && hdr[i]!='*'; i++) ;
	len -= i;
	nSkipped += i;
	memmove (hdr, hdr+i, len);
    }
}



/*
** VDS_Connect
**
** Connect over TCP/IP socket
*/
static int
VDS_Connect (void)
{
#ifdef WIN32
    WSADATA WSAdata;
    if (WSAStartup(MAKEWORD(2,0), &WSAdata) != 0) {
        fprintf (stderr, "VDS: WSAStartup ((2,0),0) => %d\n", WSAGetLastError());
        return -1;
    }
#endif

    struct sockaddr_in	DestAddr;
    struct hostent  	*he;

    if ((he=gethostbyname(VDScfg.MovieHost)) == NULL) {
        fprintf (stderr, "VDS: unknown host: %s\n", VDScfg.MovieHost);
        return -2;
    }
    DestAddr.sin_family      = AF_INET;
    DestAddr.sin_port        = htons((unsigned short)VDScfg.MoviePort);
    DestAddr.sin_addr.s_addr = *(unsigned *)he->h_addr;
    VDScfg.sock = socket(AF_INET, SOCK_STREAM, 0);
    if (connect(VDScfg.sock, (struct sockaddr *) &DestAddr, sizeof(DestAddr)) < 0) {
        fprintf (stderr, "VDS: can't connect '%s:%d'\n", VDScfg.MovieHost, VDScfg.MoviePort);
        return -4;
    }
    if (VDS_RecvHdr(VDScfg.sock, VDScfg.sbuf) < 0)
        return -3;

    printf ("VDS: Connected: %s\n", VDScfg.sbuf+1);

    return 0;
}



/*
** VDS_GetData
**
** data and image processing
*/
static int
VDS_GetData(void)
{
    int len = 0;
    int res = 0;

    /* Variables for Image Processing */
    char    ImgType[64];
    int     ImgWidth, ImgHeight, Channel;
    float   SimTime;
    int     ImgLen;
    int     nPixel;
    int     Pixel;
    int     MinDepthPixel;
    float  *f_img;

    if (sscanf(VDScfg.sbuf, "*VDS %d %s %f %dx%d %d", &Channel,
               ImgType, &SimTime, &ImgWidth, &ImgHeight, &ImgLen) == 6) {
        if (0)
            printf ("%6.3f %d: %8s %dx%d %d\n", SimTime, Channel, ImgType, ImgWidth, ImgHeight, ImgLen);
        if (ImgLen > 0) {
            char *img = (char *)malloc(ImgLen);
            for (len=0; len<ImgLen; len+=res) {
                if ((res=recv(VDScfg.sock, img+len, ImgLen-len, VDScfg.RecvFlags)) < 0) {
                    printf("VDS: Socket Reading Failure\n");
                    break;
                }
            }
	    /* This example only works with the export format "depth"
	       To use for other formats, please refer to the manual of IPGMovie,
	       where the byte format of each export format is explained.
	       For instance, for the export formt "rgb" you would be dealing
	       with:
	       		char *rgb = img;
	       where rgb[pixelNr+0] is the red component of the current pixel,
	       	     rgb[pixelNr+1] is the green component,
	       	     rgb[pixelNr+2] is the blue component,
	       and 0 <= pixelNr < (width*height), linewise, from left to right.
	    */

            if (len == ImgLen && strcmp(ImgType, "depth") == 0) {
                /* process your image here
                ** This part is just a Demo!!! ###############################
                */

                /* Starting search parameter */
                MinDepthPixel = 0;

                /* Find the pixel with the smallest distance to any object.
                ** Every pixel has a length of 4 byte float.
                */
                nPixel = ImgLen/sizeof(float);
                f_img = (float *)img;

                for (Pixel = 0; Pixel < nPixel; Pixel++) {
                    if(f_img[Pixel] < f_img[MinDepthPixel])
                        MinDepthPixel = Pixel;
                }
                VDSIF.MinDepth = f_img[MinDepthPixel];

                if (VDScfg.Verbose) {
                    /* Print general image information */
                    printf ("> %s\n", VDScfg.sbuf);

                    /* Print minimal distance and position of the pixel
                    ** x-position from left to right
                    ** y-position from top down
                    */
                    printf ("Minimal distance: %6.3f m\n", VDSIF.MinDepth);
                    printf ("Pixel position: x = %u, y = %u\n", MinDepthPixel%ImgWidth, MinDepthPixel/ImgWidth);
                    printf ("\n");
                }
                /* Demo End!!! ################################################ */
            }
            free (img);
        }
        VDSIF.nImages++;
        VDSIF.nBytes += len;
    } else {
        printf ("VDS: not handled: %s\n",VDScfg.sbuf);
    }

    return 0;
}



/*
** VDS_Init
**
** Initialize Data Struct
*/
void
VDS_Init(void)
{
    VDScfg.MovieHost = "localhost";
    VDScfg.MoviePort = 2210;
    VDScfg.Verbose   = 0;
    VDScfg.RecvFlags = 0;

    VDSIF.nImages  = 0;
    VDSIF.nBytes   = 0;
    VDSIF.MinDepth = 0;
}


int
main (int argc, char *const*argv)
{
    int i;

    VDS_Init();

    while (*++argv != NULL) {
        if (strcmp(*argv,"-v") == 0) {
	    VDScfg.Verbose++;
	} else if (strcmp(*argv,"-p")==0 && argv[1]) {
	    VDScfg.MoviePort = atoi(*++argv);
	} else if (**argv!='-') {
	    VDScfg.MovieHost = *argv;
	}
    }

    /* Connect to VDS Server */
    if ((i = VDS_Connect ()) != 0) {
        fprintf(stderr, "Can't initialise vds client (returns %d, %s)\n",
                i, i == -4 ? "No server": strerror(errno));
    }

     /* Read from TCP/IP-Port and process the image */
     while (VDS_RecvHdr(VDScfg.sock,VDScfg.sbuf) == 0) {
         VDS_GetData();
     }

    printf("Closing VDS-Client...\n");
#ifdef WIN32
    closesocket (VDScfg.sock);
#else
    close (VDScfg.sock);
#endif

    return 0;
}
