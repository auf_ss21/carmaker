
while true
    t = tcpclient("127.0.0.1",11111);


    print(t.read);
end





focalLength    = [2400, 2400]; % [fx, fy] in pixel units
principalPoint = [318.9034, 257.5352]; % [cx, cy] optical center in pixel coordinates
imageSize      = [480, 640];            % [nrows, mcols]

camIntrinsics = cameraIntrinsics(focalLength, principalPoint, imageSize);


height = 2;    % mounting height in meters from the ground
pitch  = 0;        % pitch of the camera in degrees


sensor = monoCamera(camIntrinsics, height, 'Pitch', pitch);



%###############
videoName = 'movie_new.mp4';
videoReader = VideoReader(videoName);
%Read an interesting frame that contains lane markers and a vehicle.

timeStamp = 4.06667;                   % time from the beginning of the video
videoReader.CurrentTime = timeStamp;   % point to the chosen frame

frame = readFrame(videoReader); % read frame at timeStamp seconds
imshow(frame) % display frame
%###############




% Using vehicle coordinates, define area to transform
distAheadOfSensor = 200; % in meters, as previously specified in monoCamera height input
spaceToOneSide    = 30;  % all other distance quantities are also in meters
bottomOffset      = 5;

outView   = [bottomOffset, distAheadOfSensor, -spaceToOneSide, spaceToOneSide]; % [xmin, xmax, ymin, ymax]
imageSize = [NaN, 250]; % output image width in pixels; height is chosen automatically to preserve units per pixel ratio

birdsEyeConfig = birdsEyeView(sensor, outView, imageSize);


birdsEyeImage = transformImage(birdsEyeConfig, frame);
birdsEyeImage(:,:,2) = birdsEyeImage(:,:,2) .* 1.1;
%birdsEyeImage(:,:,1) = birdsEyeImage(:,:,1) .* 0.2;
figure
imshow(birdsEyeImage)


%rgbVivid = hsv2rgb(rgb2hsv(birdsEyeImage) .* cat(3, 1, 20, 1));
%figure;
%imshow(rgbVivid);


% Convert to grayscale
birdsEyeImage = rgb2gray(birdsEyeImage);
figure
imshow(birdsEyeImage)
% Lane marker segmentation ROI in world units
vehicleROI = outView - [-1, 2, -3, 3]; % look 3 meters to left and right, and 4 meters ahead of the sensor
approxLaneMarkerWidthVehicle = 0.25; % 25 centimeters

% Detect lane features
laneSensitivity = 0.8;
birdsEyeViewBW = segmentLaneMarkerRidge(birdsEyeImage, birdsEyeConfig, approxLaneMarkerWidthVehicle,...
    'ROI', vehicleROI, 'Sensitivity', laneSensitivity);

%hold on
figure
imshow(birdsEyeViewBW)


% Obtain lane candidate points in vehicle coordinates
[imageX, imageY] = find(birdsEyeViewBW);
xyBoundaryPoints = imageToVehicle(birdsEyeConfig, [imageY, imageX]);


maxLanes      = 2; % look for maximum of two lane markers
boundaryWidth = 3*approxLaneMarkerWidthVehicle; % expand boundary width

[boundaries, boundaryPoints] = findParabolicLaneBoundaries(xyBoundaryPoints,boundaryWidth, ...
    'MaxNumBoundaries', maxLanes, 'ValidateBoundaryFcn', @ValidateBoundaryFcn);


% Establish criteria for rejecting boundaries based on their length
maxPossibleXLength = diff(vehicleROI(1:2));
minXLength         = maxPossibleXLength * 0.60; % establish a threshold

% Reject short boundaries
isOfMinLength = arrayfun(@(b)diff(b.XExtent) > minXLength, boundaries);
boundaries    = boundaries(isOfMinLength);


% To compute the maximum strength, assume all image pixels within the ROI
% are lane candidate points
birdsImageROI = vehicleToImageROI(birdsEyeConfig, vehicleROI);
[laneImageX,laneImageY] = meshgrid(birdsImageROI(1):birdsImageROI(2),birdsImageROI(3):birdsImageROI(4));

% Convert the image points to vehicle points
vehiclePoints = imageToVehicle(birdsEyeConfig,[laneImageX(:),laneImageY(:)]);

% Find the maximum number of unique x-axis locations possible for any lane
% boundary
maxPointsInOneLane = numel(unique(vehiclePoints(:,1)));

% Set the maximum length of a lane boundary to the ROI length
maxLaneLength = diff(vehicleROI(1:2));

% Compute the maximum possible lane strength for this image size/ROI size
% specification
maxStrength   = maxPointsInOneLane/maxLaneLength;

% Reject weak boundaries
isStrong      = [boundaries.Strength] > 0.4*maxStrength;
boundaries    = boundaries(isStrong);


boundaries = classifyLaneTypes(boundaries, boundaryPoints);

% Locate two ego lanes if they are present
xOffset    = 0;   %  0 meters from the sensor
distanceToBoundaries  = boundaries.computeBoundaryModel(xOffset);

% Find candidate ego boundaries
leftEgoBoundaryIndex  = [];
rightEgoBoundaryIndex = [];
minLDistance = min(distanceToBoundaries(distanceToBoundaries>0));
minRDistance = max(distanceToBoundaries(distanceToBoundaries<=0));
if ~isempty(minLDistance)
    leftEgoBoundaryIndex  = distanceToBoundaries == minLDistance;
end
if ~isempty(minRDistance)
    rightEgoBoundaryIndex = distanceToBoundaries == minRDistance;
end
leftEgoBoundary       = boundaries(leftEgoBoundaryIndex);
rightEgoBoundary      = boundaries(rightEgoBoundaryIndex);


xVehiclePoints = bottomOffset:distAheadOfSensor;
birdsEyeWithEgoLane = insertLaneBoundary(birdsEyeImage, leftEgoBoundary , birdsEyeConfig, xVehiclePoints, 'Color','Red');
birdsEyeWithEgoLane = insertLaneBoundary(birdsEyeWithEgoLane, rightEgoBoundary, birdsEyeConfig, xVehiclePoints, 'Color','Green');

frameWithEgoLane = insertLaneBoundary(frame, leftEgoBoundary, sensor, xVehiclePoints, 'Color','Red');
frameWithEgoLane = insertLaneBoundary(frameWithEgoLane, rightEgoBoundary, sensor, xVehiclePoints, 'Color','Green');
hold on
%figure
subplot('Position', [0, 0, 0.5, 1.0]) % [left, bottom, width, height] in normalized units
imshow(birdsEyeWithEgoLane)
subplot('Position', [0.5, 0, 0.5, 1.0])
imshow(frameWithEgoLane)


function isGood = ValidateBoundaryFcn(params)

if ~isempty(params)
    a = params(1);
    
    isGood = abs(a < 0.003); % not too bendy
else
    isGood = false;
end
end


function imageROI = vehicleToImageROI(birdsEyeConfig, vehicleCoordSysROI)

vehicleCoordSysROI = double(vehicleCoordSysROI);

loc2 = abs(vehicleToImage(birdsEyeConfig, [vehicleCoordSysROI(2) vehicleCoordSysROI(4)]));
loc1 = abs(vehicleToImage(birdsEyeConfig, [vehicleCoordSysROI(1) vehicleCoordSysROI(4)]));
loc4 =     vehicleToImage(birdsEyeConfig, [vehicleCoordSysROI(1) vehicleCoordSysROI(4)]);
loc3 =     vehicleToImage(birdsEyeConfig, [vehicleCoordSysROI(1) vehicleCoordSysROI(3)]);

[minRoiX, maxRoiX, minRoiY, maxRoiY] = deal(loc4(1), loc3(1), loc2(2), loc1(2));

imageROI = round([minRoiX, maxRoiX, minRoiY, maxRoiY]);

end


function boundaries = classifyLaneTypes(boundaries, boundaryPoints)

for bInd = 1 : numel(boundaries)
    vehiclePoints = boundaryPoints{bInd};
    % Sort by x
    vehiclePoints = sortrows(vehiclePoints, 1);
    
    xVehicle       = vehiclePoints(:,1);
    xVehicleUnique = unique(xVehicle);
    
    % Dashed vs Solid
    xdiff  = diff(xVehicleUnique);
    % Sufficiently large threshold to remove spaces between points of a
    % solid line, but not large enough to remove spaces between dashes
    xdifft = mean(xdiff) + 3*std(xdiff);
    largeGaps = xdiff(xdiff > xdifft);
    
    % Safe default
    boundaries(bInd).BoundaryType= LaneBoundaryType.Solid;
    if largeGaps>2
        % Ideally, these gaps should be consistent - but we cannot rely on
        % that unless we know that the ROI extent includes at least 3
        % dashes.
        boundaries(bInd).BoundaryType = LaneBoundaryType.Dashed;
    end
end

end